import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import random

x_test = pd.read_csv('x_test.csv').values
y_test = pd.read_csv('y_test.csv').values

zero = [i for i, x in enumerate(y_test) if x == 0]
one = [i for i, x in enumerate(y_test) if x == 1]
two = [i for i, x in enumerate(y_test) if x == 2]
three = [i for i, x in enumerate(y_test) if x == 3]
four = [i for i, x in enumerate(y_test) if x == 4]

selected_indexes = random.sample(four, 20)

for i in selected_indexes:
    row_of_values = x_test[i]
    pixels = np.reshape(row_of_values, (28, 28))
    plt.title(i)
    plt.imshow(pixels, cmap='gray')
    plt.show()