import numpy as np
import pandas as pd
from keras.models import Model
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Conv2D, MaxPooling2D
from sklearn.decomposition import PCA
from sklearn.cluster import KMeans, DBSCAN
from sklearn.manifold import TSNE
import matplotlib.pyplot as plt
import seaborn as sns

# Load the data
x_train = pd.read_csv('x_train.csv').values
y_train = pd.read_csv('y_train.csv').values
x_test = pd.read_csv('x_test.csv').values
y_test = pd.read_csv('y_test.csv').values

# Reshape the input data to 28x28 grayscale images
x_train = x_train.reshape(-1, 28, 28, 1)
x_test = x_test.reshape(-1, 28, 28, 1)

# Normalize the pixel values to be between 0 and 1
x_train = x_train / 255.0
x_test = x_test / 255.0

# Convert the target labels to one-hot encoding
y_train = pd.get_dummies(y_train.ravel()).values
y_test = pd.get_dummies(y_test.ravel()).values

# Define the model architecture
model = Sequential()
model.add(Conv2D(32, kernel_size=(3, 3), activation='relu', input_shape=(28, 28, 1)))
model.add(Conv2D(64, (3, 3), activation='relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.25))
model.add(Flatten())
model.add(Dense(128, activation='relu'))
model.add(Dropout(0.5))
model.add(Dense(5, activation='softmax'))

# Compile the model using the specified optimizer, loss function, and metrics
model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])

# Print the model summary
model.summary()

# Train the model
history = model.fit(x_train, y_train, batch_size=128, epochs=10, verbose=1, validation_data=(x_test, y_test))

# Evaluate the model on the test data
test_loss, test_acc = model.evaluate(x_test, y_test, verbose=0)
print('Test accuracy:', test_acc)

# Q4

# define the intermediate layer model
intermediate_layer_model = Model(inputs=model.input, outputs=model.layers[-2].output)

# get the intermediate layer output for the test data
intermediate_output = intermediate_layer_model.predict(x_test)

# convert y_test back to a single array
y_test = np.argmax(y_test, axis=1)

# perform PCA on the intermediate output
pca = PCA(n_components=2)
pca_output = pca.fit_transform(intermediate_output)

# visualize the PCA output with label colors
plt.title('PCA')
plt.scatter(pca_output[:, 0], pca_output[:, 1], c=y_test)
plt.show()

kmeans = KMeans(n_clusters=5)
dbscan = DBSCAN(eps=2, min_samples=3)

# perform clustering on the intermediate output
kmeans_output = kmeans.fit_predict(intermediate_output)
dbscan_output = dbscan.fit_predict(intermediate_output)

# visualize the clustering output with label colors
plt.title('K-MEANS')
plt.scatter(pca_output[:, 0], pca_output[:, 1], c=kmeans_output)
plt.show()

plt.title('DBScan')
plt.scatter(pca_output[:, 0], pca_output[:, 1], c=dbscan_output)
plt.show()

tsne = TSNE(n_components=2)

# perform t-SNE on the intermediate output
tsne_output = tsne.fit_transform(intermediate_output)

tsne_result_df = pd.DataFrame(
    {'tsne_1': tsne_output[:, 0], 'tsne_2': tsne_output[:, 1], 'label': y_test})
fig, ax = plt.subplots(figsize=(8, 6))
sns.scatterplot(x='tsne_1', y='tsne_2', hue='label', data=tsne_result_df, palette='Set1', ax=ax, s=40)
lim = (tsne_output.min() - 5, tsne_output.max() + 5)
ax.title.set_text('TSNE')
ax.set_xlim(lim)
ax.set_ylim(lim)
ax.set_aspect('equal')
ax.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.0)
plt.show()
