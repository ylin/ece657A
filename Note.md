# ECE 657A

- [Recordings](https://www.youtube.com/@dataandknowledgemodelingan75)

<p align="center">
  <img src="imgs/1.png" />
</p>

## Data

<p align="center">
  <img src="imgs/2.png" />
</p>

<p align="center">
  <img src="imgs/3.png" />
</p>

- Type of Data

<p align="center">
  <img src="imgs/4.png" />
</p>

<p align="center">
  <img src="imgs/5.png" />
</p>

<p align="center">
  <img src="imgs/6.png" />
</p>

## Data examination

<p align="center">
  <img src="imgs/7.png" />
</p>

<p align="center">
  <img src="imgs/8.png" />
</p>

<p align="center">
  <img src="imgs/9.png" />
</p>

- Histograms

<p align="center">
  <img src="imgs/10.png" />
</p>

- Binning method for smoothing

<p align="center">
  <img src="imgs/11.png" />
</p>

<p align="center">
  <img src="imgs/12.png" />
</p>

- Smoothing with a window

<p align="center">
  <img src="imgs/13.png" />
</p>

<p align="center">
  <img src="imgs/14.png" />
</p>

- Normalization

> Make different data in the same scale

<p align="center">
  <img src="imgs/15.png" />
</p>

> z-score normalization

<p align="center">
  <img src="imgs/16.png" />
</p>

<p align="center">
  <img src="imgs/17.png" />
</p>

<p align="center">
  <img src="imgs/18.png" />
</p>

> Min-max normalization: Guarantees all features *will have the exact same scale but does not handle outliers well*.
> Z-score normalization: Handles outliers, *but does not produce normalized data with the exact same scale*.

https://www.codecademy.com/article/normalization

- Example

<p align="center">
  <img src="imgs/19.png" />
</p>

<p align="center">
  <img src="imgs/20.png" />
</p>

- Normalization on matrix data

<p align="center">
  <img src="imgs/21.png" />
</p>

## Measuring Similarity

<p align="center">
  <img src="imgs/22.png" />
</p>

- Data matrix & Dissimilarity matrix

<p align="center">
  <img src="imgs/23.png" />
</p>

<p align="center">
  <img src="imgs/24.png" />
</p>

<p align="center">
  <img src="imgs/25.png" />
</p>

- Contingency table

<p align="center">
  <img src="imgs/26.png" />
</p>

<p align="center">
  <img src="imgs/27.png" />
</p>

<p align="center">
  <img src="imgs/28.png" />
</p>

## Distance metric

<p align="center">
  <img src="imgs/29.png" />
</p>

- How do we know that the distance is significant or revelant?

<p align="center">
  <img src="imgs/30.png" />
</p>

<p align="center">
  <img src="imgs/31.png" />
</p>

<p align="center">
  <img src="imgs/32.png" />
</p>

- Measure for ordinal typr

<p align="center">
  <img src="imgs/33.png" />
</p>

- Cosine Similarity

<p align="center">
  <img src="imgs/34.png" />
</p>

<p align="center">
  <img src="imgs/35.png" />
</p>

<p align="center">
  <img src="imgs/36.png" />
</p>

## Training & Validation

- Error estimation and training method

<p align="center">
  <img src="imgs/37.png" />
</p>

<p align="center">
  <img src="imgs/38.png" />
</p>

<p align="center">
  <img src="imgs/39.png" />
</p>

- Validation

<p align="center">
  <img src="imgs/40.png" />
</p>

- Ablation studies

<p align="center">
  <img src="imgs/41.png" />
</p>

<p align="center">
  <img src="imgs/42.png" />
</p>

<p align="center">
  <img src="imgs/43.png" />
</p>

<p align="center">
  <img src="imgs/44.png" />
</p>

<p align="center">
  <img src="imgs/45.png" />
</p>

<p align="center">
  <img src="imgs/46.png" />
</p>

<p align="center">
  <img src="imgs/47.png" />
</p>

## Supervised Learning & Unsupervised Learning

<p align="center">
  <img src="imgs/48.png" />
</p>

<p align="center">
  <img src="imgs/49.png" />
</p>

<p align="center">
  <img src="imgs/50.png" />
</p>

- Clustering & Classification

<p align="center">
  <img src="imgs/51.png" />
</p>

<p align="center">
  <img src="imgs/52.png" />
</p>

<p align="center">
  <img src="imgs/53.png" />
</p>

<p align="center">
  <img src="imgs/54.png" />
</p>

<p align="center">
  <img src="imgs/55.png" />
</p>

- Parametric & Non-parametric Model

<p align="center">
  <img src="imgs/56.png" />
</p>

<p align="center">
  <img src="imgs/57.png" />
</p>

> ***Paramerers Θ are what we are training for***

- kNN (k Nearest Neighbour Classifer) - Similarity based classifer

<p align="center">
  <img src="imgs/58.png" />
</p>

<p align="center">
  <img src="imgs/59.png" />
</p>

<p align="center">
  <img src="imgs/60.png" />
</p>

<p align="center">
  <img src="imgs/61.png" />
</p>

<p align="center">
  <img src="imgs/62.png" />
</p>

- Parzen Window Density Estimation

<p align="center">
  <img src="imgs/63.png" />
</p>

<p align="center">
  <img src="imgs/64.png" />
</p>

<p align="center">
  <img src="imgs/65.png" />
</p>

## Performance Evaluation

<p align="center">
  <img src="imgs/66.png" />
</p>

<p align="center">
  <img src="imgs/67.png" />
</p>

<p align="center">
  <img src="imgs/68.png" />
</p>

- ROC & AOC

<p align="center">
  <img src="imgs/69.png" />
</p>

- Precision vs Recall

<p align="center">
  <img src="imgs/70.png" />
</p>

<p align="center">
  <img src="imgs/71.png" />
</p>

<p align="center">
  <img src="imgs/72.png" />
</p>

<p align="center">
  <img src="imgs/73.png" />
</p>

<p align="center">
  <img src="imgs/74.png" />
</p>

- Underfitting & Overfitting

<p align="center">
  <img src="imgs/75.png" />
</p>

<p align="center">
  <img src="imgs/76.png" />
</p>

- Capacity

<p align="center">
  <img src="imgs/77.png" />
</p>

## 描述统计(Descriptive statistics)和推断统计(Inferential statistics)

- 参数估计(Parameter estimation)和假设检验(Hypothesis test) 是推断统计的两个重要内容。

- 其中参数估计又包括点估计(Point estimation)和区间估计(Interval estimation)两块内容

<p align="center">
  <img src="imgs/78.png" />
</p>

<p align="center">
  <img src="imgs/79.png" />
</p>

<p align="center">
  <img src="imgs/80.png" />
</p>

<p align="center">
  <img src="imgs/81.png" />
</p>

<p align="center">
  <img src="imgs/82.png" />
</p>

<p align="center">
  <img src="imgs/83.png" />
</p>

- example

<p align="center">
  <img src="imgs/84.png" />
</p>

<p align="center">
  <img src="imgs/85.png" />
</p>

<p align="center">
  <img src="imgs/86.png" />
</p>

> https://www.youtube.com/watch?v=p7ccVRKyhIE&ab_channel=MathAdamSpiegler

- Mean squared error

<p align="center">
  <img src="imgs/87.png" />
</p>

<p align="center">
  <img src="imgs/88.png" />
</p>

## [Maximum Likelihood Estimation (MLE) & Maximum a Posteriori (MAP) Estimation](https://www.jianshu.com/p/9c153d82ba2d)

- MLE

**其中i.i.d. 表示 Independent and identical distribution，独立同分布**

> Assume we know the data, we want to fit distribution to it -> Probabilistic ways to compute parameters for the distribution that we are trying to fit data to

<p align="center">
  <img src="imgs/89.png" />
</p>

<p align="center">
  <img src="imgs/120.png" />
</p>

> 什么是似然函数？

<p align="center">
  <img src="imgs/90.png" />
</p>

<p align="center">
  <img src="imgs/91.png" />
</p>

<p align="center">
  <img src="imgs/92.png" />
</p>

> [MLE 推导](https://www.zhihu.com/question/360418580/answer/931462150)

- Example

<p align="center">
  <img src="imgs/93.png" />
</p>

<p align="center">
  <img src="imgs/94.png" />
</p>

<p align="center">
  <img src="imgs/95.png" />
</p>

<p align="center">
  <img src="imgs/96.png" />
</p>

<p align="center">
  <img src="imgs/97.png" />
</p>

- Limitations

<p align="center">
  <img src="imgs/98.png" />
</p>

<p align="center">
  <img src="imgs/111.png" />
</p>

From: https://zhuanlan.zhihu.com/p/345024301 <br/>
From: https://www.zhihu.com/question/54082000 <br/>
From: https://www.zhihu.com/question/360418580/answer/931462150 <br/>
From: https://mp.weixin.qq.com/s?__biz=MzI4MDYzNzg4Mw==&mid=2247552799&idx=3&sn=87cdfb929865571ec96738c42deb3240&chksm=ebb737cbdcc0bedd74395a68fb76796f1c102a6129ee2e81e0932da786ec58450d415a448a6e&scene=27<br/>

- MAP

<p align="center">
  <img src="imgs/99.png" />
</p>

<p align="center">
  <img src="imgs/100.png" />
</p>

<p align="center">
  <img src="imgs/121.png" />
</p>

- [Linear regression](https://www.zhihu.com/question/304164704/answer/1474361415)

<p align="center">
  <img src="imgs/101.png" />
</p>

- [Logistic Regression](https://www.jianshu.com/p/b603be6c9f0d)

<p align="center">
  <img src="imgs/102.png" />
</p>

<p align="center">
  <img src="imgs/103.png" />
</p>

<p align="center">
  <img src="imgs/104.png" />
</p>

- [Cost function & Risk function & Target function](https://www.zhihu.com/question/52398145)

<p align="center">
  <img src="imgs/105.png" />
</p>

<p align="center">
  <img src="imgs/106.png" />
</p>

> https://zhuanlan.zhihu.com/p/124757082

<p align="center">
  <img src="imgs/107.png" />
</p>

<p align="center">
  <img src="imgs/108.png" />
</p>

<p align="center">
  <img src="imgs/109.png" />
</p>

<p align="center">
  <img src="imgs/110.png" />
</p>

<p align="center">
  <img src="imgs/112.png" />
</p>

<p align="center">
  <img src="imgs/113.png" />
</p>

## [Naive Bayes Classifier](https://zhuanlan.zhihu.com/p/52728190) and Expectation Maximization

<p align="center">
  <img src="imgs/114.png" />
</p>

<p align="center">
  <img src="imgs/115.png" />
</p>

<p align="center">
  <img src="imgs/116.png" />
</p>

<p align="center">
  <img src="imgs/117.png" />
</p>

<p align="center">
  <img src="imgs/118.png" />
</p>

<p align="center">
  <img src="imgs/119.png" />
</p>

- [先验与后验](https://www.jianshu.com/p/0aeae4d82daa)

- Missing data - Expectation Maximization

<p align="center">
  <img src="imgs/122.png" />
</p>

<p align="center">
  <img src="imgs/123.png" />
</p>

> [MLE for normal distribution](https://towardsdatascience.com/maximum-likelihood-estimation-explained-normal-distribution-6207b322e47f#:~:text=MLE%20tells%20us%20which%20curve,are%20our%20parameters%20of%20interest.)

<p align="center">
  <img src="imgs/124.png" />
</p>

<p align="center">
  <img src="imgs/125.png" />
</p>