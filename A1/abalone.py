import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import cross_val_score

# fixed seed for reproducibility

if __name__ == "__main__":
    # 0. pre-process raw data
    # load data
    features = ['Sex', 'Length', 'Diameter', 'Height', 'Whole', 'Shucked', 'Viscera', 'Shell']

    label = ['Rings']
    df = pd.read_csv('./abalone.csv', names=features + label)

    # Q1.1 check missing values
    misses = df.isna().sum().sum()
    if misses == 0:
        print('No missing values.', end='\n\n')
    else:
        print('Missing ' + misses + ' values.', end='\n\n')

    # Q1.2 Data moments
    print('mean:')
    print(df.iloc[:, :-1].mean(numeric_only=True), end='\n\n')
    print('median:')
    print(df.iloc[:, :-1].median(numeric_only=True), end='\n\n')
    print('variance:')
    print(df.iloc[:, :-1].var(numeric_only=True), end='\n\n')
    print('skew:')
    print(df.iloc[:, :-1].skew(numeric_only=True), end='\n\n')
    print('kurtosis:')
    print(df.iloc[:, :-1].kurt(numeric_only=True), end='\n\n')

    # Q1.3 Pairplot over the features
    sns.pairplot(df[features])
    plt.show()

    # Q1.4 Determine if the dataset is balanced
    print(df[label].value_counts())
    print('Number of records for each Ring class is not uniform, so the dataset is NOT balanced.')

    # Q1.5 Normalize data using z-score
    # Sex is converted into dummies as it is not quantitative
    # Rings is not normalized as it is the label
    sex_dummies = pd.get_dummies(df['Sex'], prefix='Sex')
    features = sex_dummies.columns.tolist() + features[1:]
    df = sex_dummies.join(df).drop('Sex', axis=1)
    StandardScaler(copy=False).fit_transform(df.values[:, :-1])

    # Q2.4 Split data into train and test sets
    X_train, X_test, y_train, y_test = train_test_split(df[features], df[label], test_size=0.2)

    # Q2.1 Classify using KNN on train set using default parameters
    knc = KNeighborsClassifier()
    knc.fit(X_train, y_train.values.ravel())

    # Q2.2 This dataset leads to low KNN accuracy
    print(knc.score(X_test, y_test))

    # Q2.5 5-fold cross validation on the training set over a range of Ks
    k = []
    accuracy = []
    accuracy_ed = []
    accuracy_md = []
    for x in range(1, 51):
        knc_cv = KNeighborsClassifier(n_neighbors=x)
        knc_cv_ed = KNeighborsClassifier(n_neighbors=x, metric='euclidean', weights='distance')
        knc_cv_md = KNeighborsClassifier(n_neighbors=x, metric='manhattan', weights='distance')
        cv_scores = cross_val_score(knc_cv, X_train, y_train.values.ravel(), cv=5)
        cv_ed_scores = cross_val_score(knc_cv_ed, X_train, y_train.values.ravel(), cv=5)
        cv_md_scores = cross_val_score(knc_cv_md, X_train, y_train.values.ravel(), cv=5)
        k.append(x)
        accuracy.append(np.mean(cv_scores))
        accuracy_ed.append(np.mean(cv_ed_scores))
        accuracy_md.append(np.mean(cv_md_scores))
        # print(cv_scores)
        # print('cv_scores mean:{}'.format(np.mean(cv_scores)))

    # Q2.6 Plot mean validation accuracy vs k, also for ED and MD mentioned in Q2.8.2
    ka = np.array(k)
    aa = np.array(accuracy)
    aa_ed = np.array(accuracy_ed)
    aa_md = np.array(accuracy_md)
    # Plot for regular KNN
    plt.figure(dpi=100)
    plt.plot(ka, aa)
    plt.title('Mean Validation Accuracy vs K')
    plt.xlabel('K')
    plt.ylabel('Mean Validation Accuracy')
    plt.show()
    # Plot for Weighted KNN using ED
    plt.figure(dpi=100)
    plt.plot(ka, aa_ed)
    plt.title('Mean Validation Accuracy vs K : Using Euclidean Distance')
    plt.xlabel('K')
    plt.ylabel('Mean Validation Accuracy')
    plt.show()
    # Plot for Weighted KNN using MD
    plt.figure(dpi=100)
    plt.plot(ka, aa_md)
    plt.title('Mean Validation Accuracy vs K : Using Manhattan Distance')
    plt.xlabel('K')
    plt.ylabel('Mean Validation Accuracy')
    plt.show()

    # Q2.7 Retrain KNN with the selected K
    best_k = np.argmax(accuracy).item()
    print('The best K according to the experiments is: ', best_k)
    knc = KNeighborsClassifier(n_neighbors=best_k)
    knc.fit(X_train, y_train.values.ravel())
    print(knc.score(X_test, y_test))

    # Q2.8 Weighted KNN using distance
    # ED
    best_k = np.argmax(accuracy_ed).item()
    print('The best K for Weighted KNN using Euclidean distance is: ', best_k)
    knc = KNeighborsClassifier(n_neighbors=best_k, metric='euclidean', weights='distance')
    knc.fit(X_train, y_train.values.ravel())
    print(knc.score(X_test, y_test))
    # MD
    best_k = np.argmax(accuracy_md).item()
    print('The best K for Weighted KNN using Manhattan distance is: ', best_k)
    knc = KNeighborsClassifier(n_neighbors=best_k, metric='manhattan', weights='distance')
    knc.fit(X_train, y_train.values.ravel())
    print(knc.score(X_test, y_test))
