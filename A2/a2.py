import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import cross_val_score
from sklearn.decomposition import PCA
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.metrics import accuracy_score
from sklearn.manifold import TSNE

# 1, 2
#############################################################################################################

# Pre-process raw data - Abalone
# load data
features_abalone = ['Sex', 'Length', 'Diameter',
                    'Height', 'Whole', 'Shucked', 'Viscera', 'Shell']

label_abalone = ['Rings']
df_abalone = pd.read_csv(
    './abalone.csv', names=features_abalone + label_abalone)

# For now remove 'Sex' column
df_abalone = df_abalone.drop(columns=['Sex'])

# Pre-process raw data - Wine
# load data
features_wine = ['fixed acidity', 'volatile acidity', 'citric acid', 'residual sugar', 'chlorides',
                 'free sulfur dioxide', 'total sulfur dioxide', 'density', 'pH', 'sulphates', 'alcohol', 'colour']

label_wine = ['quality']

# Red
df_wine_r = pd.read_csv("./winequality-red.csv", sep=';')
df_wine_r["colour"] = 1

# White
df_wine_w = pd.read_csv("./winequality-white.csv",
                        sep=';')
df_wine_w["colour"] = 0

# Combine
df_wine_combine = pd.concat([df_wine_r, df_wine_w])


# # ------------------------------- Abalone

# # Normalize data using z-score - Abalone

# # Sex is converted into dummies as it is not quantitative
# # Rings is not normalized as it is the label
# # sex_dummies = pd.get_dummies(df['Sex'], prefix='Sex')
# # features = sex_dummies.columns.tolist() + features[1:]
# # df = sex_dummies.join(df).drop('Sex', axis=1)

# normalized_data_abalone = StandardScaler(
#     copy=False).fit_transform(df_abalone.values[:, :-1])

# # print(df_abalone.tail())
# # print(normalized_data_abalone)

# # PCA - Abalone - Original dimension
# pca_abalone = PCA(n_components=len(features_abalone) - 1)
# principalComponents_abalone = pca_abalone.fit_transform(
#     normalized_data_abalone)
# principalComponents_abalone_df = pd.DataFrame(data=principalComponents_abalone)

# print(principalComponents_abalone_df.tail())
# print('Explained variation per principal component: {}'.format(
#     pca_abalone.explained_variance_ratio_))

# plt.figure()
# plt.figure(figsize=(10, 10))
# plt.xticks(fontsize=12)
# plt.yticks(fontsize=14)
# plt.xlabel('Principal Component - 1', fontsize=20)
# plt.ylabel('Principal Component - 2', fontsize=20)
# plt.title("Principal Component Analysis of Abalone Dataset", fontsize=20)

# sns.scatterplot(x=principalComponents_abalone_df.values[:, 0],
#                 y=principalComponents_abalone_df.values[:, 1], hue=df_abalone['Rings'])

# plt.show()

# # Scree Plot - Abalone - PCA
# pc_values_abalone_pca = np.arange(pca_abalone.n_components_) + 1
# plt.title('Scree Plot')
# plt.xlabel('Principal Component')
# plt.ylabel('Proportion of Variance Explained - Abalone - PCA')
# plt.plot(pc_values_abalone_pca, pca_abalone.explained_variance_ratio_,
#          'ro-', linewidth=2)
# plt.show()

# # LDA - Abalone - Original dimension
# # ValueError: n_components cannot be larger than min(n_features, n_classes - 1)
# # n_classes - 1 axis for LDA
# lda_abalone = LinearDiscriminantAnalysis(
#     n_components=len(features_abalone) - 1)
# # Label aware
# lda_abalone_point_data = lda_abalone.fit_transform(
#     normalized_data_abalone, df_abalone['Rings'])
# lda_abalone_point_data_df = pd.DataFrame(data=lda_abalone_point_data)

# print(lda_abalone_point_data_df.tail())
# print('Explained variation per dimension: {}'.format(
#     lda_abalone.explained_variance_ratio_))

# plt.figure()
# plt.figure(figsize=(10, 10))
# plt.xticks(fontsize=12)
# plt.yticks(fontsize=14)
# plt.xlabel('LDA - 1', fontsize=20)
# plt.ylabel('LDA - 2', fontsize=20)
# plt.title("Linear Discriminant Analysis of Abalone Dataset", fontsize=20)

# sns.scatterplot(x=lda_abalone_point_data_df.values[:, 0],
#                 y=lda_abalone_point_data_df.values[:, 1], hue=df_abalone['Rings'])

# plt.show()

# # Scree Plot - Abalone - LDA
# pc_values_abalone_lda = np.arange(lda_abalone.n_components) + 1
# plt.title('Scree Plot')
# plt.xlabel('Dimension')
# plt.ylabel('Proportion of Variance Explained - Abalone - LDA')
# plt.plot(pc_values_abalone_lda, lda_abalone.explained_variance_ratio_,
#          'ro-', linewidth=2)
# plt.show()

# # ------------------------------- Wine

# # Normalize data using z-score - Wine
# normalized_data_wine = StandardScaler(
#     copy=False).fit_transform(df_wine_combine.loc[:, df_wine_combine.columns != 'quality'].values)

# # print(df_wine_combine.loc[:, df_wine_combine.columns != 'quality'].head())

# # PCA - Wine - Original dimension
# pca_wine = PCA(n_components=len(features_wine))
# principalComponents_wine = pca_wine.fit_transform(normalized_data_wine)
# principalComponents_wine_df = pd.DataFrame(data=principalComponents_wine)

# print(principalComponents_wine_df.tail())
# print('Explained variation per principal component: {}'.format(
#     pca_wine.explained_variance_ratio_))

# plt.figure()
# plt.figure(figsize=(10, 10))
# plt.xticks(fontsize=12)
# plt.yticks(fontsize=14)
# plt.xlabel('Principal Component - 1', fontsize=20)
# plt.ylabel('Principal Component - 2', fontsize=20)
# plt.title("Principal Component Analysis of Wine Dataset", fontsize=20)

# sns.scatterplot(x=principalComponents_wine_df.values[:, 0],
#                 y=principalComponents_wine_df.values[:, 1], hue=df_wine_combine['quality'])

# plt.show()

# # Scree Plot - Wine - PCA
# pc_values_wine_pca = np.arange(pca_wine.n_components_) + 1
# plt.title('Scree Plot')
# plt.xlabel('Principal Component')
# plt.ylabel('Proportion of Variance Explained - Wine - PCA')
# plt.plot(pc_values_wine_pca, pca_wine.explained_variance_ratio_, 'ro-', linewidth=2)
# plt.show()

# # LDA - Wine - Original dimension
# # ValueError: n_components cannot be larger than min(n_features, n_classes - 1)
# # n_classes - 1 axis for LDA
# lda_wine = LinearDiscriminantAnalysis(
#     n_components=len(df_wine_combine['quality'].unique()) - 1)
# # Label aware
# lda_wine_point_data = lda_wine.fit_transform(
#     normalized_data_wine, df_wine_combine['quality'])
# lda_wine_point_data_df = pd.DataFrame(data=lda_wine_point_data)

# print(lda_wine_point_data_df.tail())
# print('Explained variation per dimension: {}'.format(
#     pca_wine.explained_variance_ratio_))

# plt.figure()
# plt.figure(figsize=(10, 10))
# plt.xticks(fontsize=12)
# plt.yticks(fontsize=14)
# plt.xlabel('LDA - 1', fontsize=20)
# plt.ylabel('LDA - 2', fontsize=20)
# plt.title("Linear Discriminant Analysis of Wine Dataset", fontsize=20)

# sns.scatterplot(x=lda_wine_point_data_df.values[:, 0],
#                 y=lda_wine_point_data_df.values[:, 1], hue=df_wine_combine['quality'])

# plt.show()

# # Scree Plot - Wine - LDA
# pc_values_wine_lda = np.arange(lda_wine.n_components) + 1
# plt.title('Scree Plot')
# plt.xlabel('Dimension')
# plt.ylabel('Proportion of Variance Explained - Wine - LDA')
# plt.plot(pc_values_wine_lda, lda_wine.explained_variance_ratio_, 'ro-', linewidth=2)
# plt.show()

# 3
#############################################################################################################

# ------- Abalone

# Abalone - Split data into train and test sets - features (x) and label (y)
X_train_abalone, X_test_abalone, y_train_abalone, y_test_abalone = train_test_split(
    df_abalone.loc[:, df_abalone.columns != 'Rings'], df_abalone[label_abalone], test_size=0.2, random_state=42)

sc_abalone = StandardScaler()
X_train_abalone = sc_abalone.fit_transform(X_train_abalone)
X_test_abalone = sc_abalone.transform(X_test_abalone)

# PCA

# plt.figure(dpi=100)
# plt.title('Using Manhattan Distance - Draw different dimensions with different k')
# plt.xlabel('K')
# plt.ylabel('Mean Validation Accuracy')

# # loop dimension
# for dimen in range(1, len(features_abalone)):
#     pca_abalone = PCA(n_components=dimen)
#     principalComponents_abalone_train = pca_abalone.fit_transform(
#         X_train_abalone)
#     principalComponents_abalone_test = pca_abalone.transform(X_test_abalone)

#     # Loop k
#     # Record test accuracy
#     k_max_range = 36
#     k = []
#     accuracy_md = []
#     for x in range(1, k_max_range):
#         knc_cv_md = KNeighborsClassifier(
#             n_neighbors=x, metric='manhattan', weights='distance')
#         knc_cv_md.fit(principalComponents_abalone_train,
#                       y_train_abalone.values.ravel())

#         k.append(x)
#         accuracy_md.append(knc_cv_md.score(
#             principalComponents_abalone_test, y_test_abalone))

#     ka = np.array(k)
#     aa_md = np.array(accuracy_md)
#     plt.plot(ka, aa_md, label=dimen)

# plt.legend(title="Dimension")
# plt.show()

# LDA

# plt.figure(dpi=100)
# plt.title('Using Manhattan Distance - Draw different dimensions with different k')
# plt.xlabel('K')
# plt.ylabel('Mean Validation Accuracy')

# # loop dimension
# for dimen in range(1, len(features_abalone)):
#     lda_abalone = LinearDiscriminantAnalysis(n_components=dimen)
#     lda_abalone_train = lda_abalone.fit_transform(
#         X_train_abalone, y_train_abalone.values.ravel())
#     lda_abalone_test = lda_abalone.transform(X_test_abalone)

#     # Loop k
#     # Record test accuracy
#     k_max_range = 36
#     k = []
#     accuracy_md = []
#     for x in range(1, k_max_range):
#         knc_cv_md = KNeighborsClassifier(
#             n_neighbors=x, metric='manhattan', weights='distance')
#         knc_cv_md.fit(lda_abalone_train,
#                       y_train_abalone.values.ravel())

#         k.append(x)
#         accuracy_md.append(knc_cv_md.score(lda_abalone_test, y_test_abalone))

#     ka = np.array(k)
#     aa_md = np.array(accuracy_md)
#     plt.plot(ka, aa_md, label=dimen)

# plt.legend(title="Dimension")
# plt.show()

# ------- Wine

# Wine - Split data into train and test sets
X_train_wine, X_test_wine, y_train_wine, y_test_wine = train_test_split(
    df_wine_combine[features_wine], df_wine_combine[label_wine], test_size=0.2, random_state=42)

sc_wine = StandardScaler()
X_train_wine = sc_wine.fit_transform(X_train_wine)
X_test_wine = sc_wine.transform(X_test_wine)

# PCA

# plt.figure(dpi=100)
# plt.title('Using Manhattan Distance - Draw different dimensions with different k')
# plt.xlabel('K')
# plt.ylabel('Mean Validation Accuracy')

# # loop dimension
# for dimen in range(1, len(features_wine) + 1):
#     pca_wine = PCA(n_components=dimen)
#     principalComponents_wine_train = pca_wine.fit_transform(
#         X_train_wine)
#     principalComponents_wine_test = pca_wine.transform(X_test_wine)

#     # Loop k
#     # Record test accuracy
#     k_max_range = 36
#     k = []
#     accuracy_md = []
#     for x in range(1, k_max_range):
#         knc_cv_md = KNeighborsClassifier(
#             n_neighbors=x, metric='manhattan', weights='distance')
#         knc_cv_md.fit(principalComponents_wine_train,
#                       y_train_wine.values.ravel())

#         k.append(x)
#         accuracy_md.append(knc_cv_md.score(
#             principalComponents_wine_test, y_test_wine))

#     ka = np.array(k)
#     aa_md = np.array(accuracy_md)
#     plt.plot(ka, aa_md, label=dimen)

# plt.legend(title="Dimension")
# plt.show()

# LDA

# plt.figure(dpi=100)
# plt.title('Using Manhattan Distance - Draw different dimensions with different k')
# plt.xlabel('K')
# plt.ylabel('Mean Validation Accuracy')

# # loop dimension
# for dimen in range(1, len(df_wine_combine['quality'].unique())):
#     lda_wine = LinearDiscriminantAnalysis(n_components=dimen)
#     lda_wine_train = lda_wine.fit_transform(
#         X_train_wine, y_train_wine.values.ravel())
#     lda_wine_test = lda_wine.transform(X_test_wine)

#     # Loop k
#     # Record test accuracy
#     k_max_range = 36
#     k = []
#     accuracy_md = []
#     for x in range(1, k_max_range):
#         knc_cv_md = KNeighborsClassifier(
#             n_neighbors=x, metric='manhattan', weights='distance')
#         knc_cv_md.fit(lda_wine_train,
#                       y_train_wine.values.ravel())

#         k.append(x)
#         accuracy_md.append(knc_cv_md.score(lda_wine_test, y_test_wine))

#     ka = np.array(k)
#     aa_md = np.array(accuracy_md)
#     plt.plot(ka, aa_md, label=dimen)

# plt.legend(title="Dimension")
# plt.show()

# 4
#############################################################################################################

# ------- Abalone

# normalized_data_abalone = StandardScaler(
#     copy=False).fit_transform(df_abalone.values[:, :-1])

# # We want to get TSNE embedding with 2 dimensions
# n_components = 2
# tsne_abalone = TSNE(n_components)
# tsne_abalone_result = tsne_abalone.fit_transform(normalized_data_abalone)

# # Plot the result of our TSNE with the label color coded

# tsne_abalone_result_df = pd.DataFrame(
#     {'tsne_1': tsne_abalone_result[:, 0], 'tsne_2': tsne_abalone_result[:, 1], 'label': df_abalone['Rings']})
# fig, ax = plt.subplots(1)
# sns.scatterplot(x='tsne_1', y='tsne_2', hue='label',
#                 data=tsne_abalone_result_df, ax=ax, s=120)
# lim = (tsne_abalone_result.min()-5, tsne_abalone_result.max()+5)
# ax.set_xlim(lim)
# ax.set_ylim(lim)
# ax.set_aspect('equal')
# ax.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.0)
# ax.title.set_text('TSNE - Abalone - 2D')

# plt.show()

# ------- Wine

normalized_data_wine = StandardScaler(
    copy=False).fit_transform(df_wine_combine.loc[:, df_wine_combine.columns != 'quality'].values)

# We want to get TSNE embedding with 2 dimensions
n_components = 2
tsne_wine = TSNE(n_components)
tsne_wine_result = tsne_wine.fit_transform(normalized_data_wine)

# Plot the result of our TSNE with the label color coded
tsne_wine_result_df = pd.DataFrame(
    {'tsne_1': tsne_wine_result[:, 0], 'tsne_2': tsne_wine_result[:, 1], 'label': df_wine_combine['quality']})
fig, ax = plt.subplots(1)
sns.scatterplot(x='tsne_1', y='tsne_2', hue='label',
                data=tsne_wine_result_df, ax=ax, s=120)
lim = (tsne_wine_result.min()-5, tsne_wine_result.max()+5)
ax.set_xlim(lim)
ax.set_ylim(lim)
ax.set_aspect('equal')
ax.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.0)
ax.title.set_text('TSNE - Wine - 2D')

plt.show()

#############################################################################################################
# curl -X POST -H "Content-Type: application/json" -d '{"user_id": "jjj", "password": "1323"}' 127.0.0.1:5000/auth
# morgon - text
# sql
# api goole doc link
